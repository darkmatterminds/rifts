-- This file is provided under the Open Game License version 1.0a
-- For more information on OGL and related issues, see 
--   http://www.wizards.com/d20
--
-- For information on the Fantasy Grounds d20 Ruleset licensing and
-- the OGL license text, see the d20 ruleset license in the program
-- options.
--
-- All producers of work derived from this definition are adviced to
-- familiarize themselves with the above licenses, and to take special
-- care in providing the definition of Product Identity (as specified
-- by the OGL) in their products.
--
-- Copyright 2008 SmiteWorks Ltd.

function onInit()
	if not User.isLocal() then
		if User.isHost() then
			DesktopManager.registerStackShortcut("button_light", "button_light_down", "Lighting", "lightingselection");
			DesktopManager.registerStackShortcut("button_pointer", "button_pointer_down", "Colors", "pointerselection");
			DesktopManager.registerStackShortcut("button_tracker", "button_tracker_down", "Combat tracker", "combattracker");
			DesktopManager.registerStackShortcut("button_characters", "button_characters_down", "Characters", "charactersheetlist", "charsheet");
			DesktopManager.registerStackShortcut("button_modules", "button_modules_down", "Modules", "moduleselection");
			
			DesktopManager.registerDockShortcut("button_book", "button_book_down", "Story", "encounterlist", "encounter");
			DesktopManager.registerDockShortcut("button_maps", "button_maps_down", "Maps & Images", "imagelist", "image");
			DesktopManager.registerDockShortcut("button_people", "button_people_down", "Personalities", "npclist", "npc");
			DesktopManager.registerDockShortcut("button_itemchest", "button_itemchest_down", "Items", "itemlist", "item");
			DesktopManager.registerDockShortcut("button_library", "button_library_down", "Library", "library");
			
			DesktopManager.registerDockShortcut("button_tokencase", "button_tokencase_down", "Tokens", "tokenbag", nil, true);
		else
			DesktopManager.registerStackShortcut("button_portraits", "button_portraits_down", "Portraits", "portraitselection");
			DesktopManager.registerStackShortcut("button_pointer", "button_pointer_down", "Colors", "pointerselection");
			DesktopManager.registerStackShortcut("button_characters", "button_characters_down", "Characters", "identityselection");
			DesktopManager.registerStackShortcut("button_modules", "button_modules_down", "Modules", "moduleselection");
			
			DesktopManager.registerDockShortcut("button_notes", "button_notes_down", "Notes", "notelist", "notes");
			DesktopManager.registerDockShortcut("button_library", "button_library_down", "Library", "library");
			
			DesktopManager.registerDockShortcut("button_tokencase", "button_tokencase_down", "Tokens", "tokenbag", nil, true);
		end
	else
		DesktopManager.registerStackShortcut("button_characters", "button_characters_down", "Characters", "identityselection");
		DesktopManager.registerStackShortcut("button_pointer", "button_pointer_down", "Colors", "pointerselection");
		DesktopManager.registerStackShortcut("button_portraits", "button_portraits_down", "Portraits", "portraitselection");
		DesktopManager.registerStackShortcut("button_modules", "button_modules_down", "Modules", "moduleselection");

		DesktopManager.registerDockShortcut("button_library", "button_library_down", "Library", "library");
	end
end